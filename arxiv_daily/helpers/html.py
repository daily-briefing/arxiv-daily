from pathlib import Path
from bs4 import BeautifulSoup as BS
import datetime
import os

HERE = os.path.dirname(__file__)
HTML_DIRECTORY = f'{Path.home()}/.arxiv_daily/html'
TR_STYLE = 'border-bottom: 1px solid rgba(0, 0, 0, 0.05); --darkreader-inline-border-bottom:rgba(19, 21, 22, 0.05);'


def _init():
    Path(HTML_DIRECTORY).mkdir(parents=True, exist_ok=True)


def _strongify(text, keywords):
    strong_text = text
    for k in keywords:
        strong_text = strong_text.replace(k, f"<strong>{k}</strong>")

    return strong_text


def write(papers, keywords=[], authors=[]):
    _init()

    with open(f'{HERE}/html_assets/template.html', 'r') as f:
        contents = f.read()
        soup = BS(contents, 'lxml')

    today = datetime.datetime.now()

    fname = today.strftime('%Y-%m-%d-%H-%M-%s.html')

    datetime_tag = soup.select_one('#mydatetime')
    datetime_tag.append(today.strftime('%d, %b %Y'))

    signature_tag = soup.select_one('#mysignature')
    signature_tag.append('Saeed Sarpas')

    table_tag = soup.select_one('#mytable')

    for i, p in enumerate(papers):
        tr = soup.new_tag(
            'tr', style=TR_STYLE,
            **{"data-darkreader-inline-border-bottom": ""}
        )

        th = soup.new_tag(
            'th', style="text-align:left; padding: 0 2.5em;",
            **{'width': '80%', 'valign': 'middle'}
        )

        container_tag = soup.new_tag('div', **{'class': 'product-entry'})

        div_tag = soup.new_tag('div', **{"class": "text"})

        timestamp_tag = soup.new_tag('h5')
        upd_time = p['time']['updated'].strftime('%d, %b %Y')
        pub_time = f"({p['time']['published'].strftime('%d, %b %Y')})" if 'published' in p['time'] else ''
        timestamp_tag.append(f"{upd_time} {pub_time}")

        title_tag = soup.new_tag('h3')
        title_tag.append(BS(_strongify(p['title'], keywords), 'html.parser'))

        authors_tag = soup.new_tag('span')
        authors_tag.append(
            BS(_strongify(', '.join(p['authors']), authors), 'html.parser'))

        p_tag = soup.new_tag('p')
        p_tag.append(BS(_strongify(p['summary'], keywords), 'html.parser'))

        div_tag.insert(1, timestamp_tag)
        div_tag.insert(2, title_tag)
        div_tag.insert(3, authors_tag)

        container_tag.insert(1, div_tag)
        container_tag.insert(2, p_tag)

        th.insert(1, container_tag)
        tr.insert(1, th)
        table_tag.insert(i+1, tr)

    with open(f"{HTML_DIRECTORY}/{fname}", 'w+') as f:
        f.write(soup.prettify())

    return f"{HTML_DIRECTORY}/{fname}"
