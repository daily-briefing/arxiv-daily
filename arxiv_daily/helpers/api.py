import requests
import xmltodict
from datetime import datetime
import re
import pickle


ARXIV_API_URL = "http://export.arxiv.org/api"

CATEGORIES = [
    "astro-ph",
    "astro-ph.CO",
    "astro-ph.EP",
    "astro-ph.GA",
    "astro-ph.HE",
    "astro-ph.IM",
    "astro-ph.SR",
]


def _parse_paper(entry):
    timestamp = "%Y-%m-%dT%H:%M:%SZ"
    pid = re.sub(r'v[1-9]+', '', entry['id'].split('/')[-1])

    if len(re.split(r'v', entry['id'].split('/')[-1])) > 1:
        try:
            version = int(re.split(r'v', entry['id'].split('/')[-1])[-1])
        except:
            version = -1
    else:
        version = 0

    parsed = {
        "id": pid,
        "version": version,
        "title": entry['title'],
        "authors_orig": entry['author'],
        "summary": entry['summary'].replace('\n', ' '),
        "time": {
            "updated": datetime.strptime(entry['updated'], timestamp),
            "published": datetime.strptime(entry['published'], timestamp),
        },
        "link": {
            e['@title']: e['@href'] for e in entry['link']
            if '@title' in e and '@href' in e
        },
    }

    parsed['link']['id'] = entry['id']

    parsed['authors'] = []
    if len(entry['author']) == 1:
        entry['author'] = [entry['author']]

    for a in entry['author']:
        try:
            parsed['authors'].append(a['name'])
        except:
            parsed['authors'].append('')

    if "arxiv:doi" in entry:
        parsed['comment'] = entry['arxiv:doi']["#text"]

    if "arxiv:comment" in entry:
        c = entry['arxiv:comment']["#text"]

        try:
            pages = int(re.search(r'\d*\spage', c).group().split(' ')[0])
        except:
            pages = -1

        try:
            figures = int(re.search(r'\d*\sfigure', c).group().split(' ')[0])
        except:
            figures = -1

        parsed['pages'] = pages
        parsed['figrues'] = figures
        parsed['comment'] = c

    if "arxiv:journal_ref" in entry:
        parsed['journal'] = entry['arxiv:journal_ref']["#text"]

    return parsed


def get(page=1, length=2, cats=CATEGORIES):
    query = ARXIV_API_URL + \
        f"/query?search_query={'+OR+'.join(cats)}" + \
        f"&start={page-1}" + \
        f"&max_results={length}" + \
        "&sortBy=lastUpdatedDate&sortOrder=descending"

    try:
        res = requests.get(query)
        papers_raw = xmltodict.parse(res.content)['feed']['entry']
    except:
        papers_raw = []

    papers = [_parse_paper(x) for x in papers_raw]

    # Testing DB
    #  with open("/scratch/saeed/.arxiv_daily/papers.db", 'wb') as dbfile:
    #      pickle.dump(papers, dbfile)
    #  with open("/scratch/saeed/.arxiv_daily/papers.db", "rb") as dbfile:
    #      papers = pickle.load(dbfile)

    return papers
