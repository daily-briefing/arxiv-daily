from pathlib import Path
import pickle


DB_DIRECTORY = f'{Path.home()}/.arxiv_daily/db'
AUTHORS_DB_NAME = "Author.db"
PAPERS_DB_NAME = "Papers.db"


def _setup_db_directory():
    try:
        Path(DB_DIRECTORY).mkdir(parents=True, exist_ok=True)
        return True
    except:
        return False


def _init():
    if not _setup_db_directory():
        return False

    return True


def get_papers():
    if not Path(f"{DB_DIRECTORY}/{PAPERS_DB_NAME}").is_file():
        return False, {}

    try:
        with open(f"{DB_DIRECTORY}/{PAPERS_DB_NAME}", "rb") as dbfile:
            papers = pickle.load(dbfile)

        return True, papers
    except:
        return False, {}


def get_authors():
    if not Path(f"{DB_DIRECTORY}/{AUTHORS_DB_NAME}").is_file():
        return False, {}

    try:
        with open(f"{DB_DIRECTORY}/{AUTHORS_DB_NAME}", "rb") as dbfile:
            authors = pickle.load(dbfile)
            return True, authors
    except:
        return False, {}


def store_papers(papers):
    if not _init():
        return False

    try:
        with open(f"{DB_DIRECTORY}/{PAPERS_DB_NAME}", 'wb') as dbfile:
            pickle.dump(papers, dbfile)
        return True
    except:
        return False


def store_authors(authors):
    if not _init():
        return False

    try:
        with open(f"{DB_DIRECTORY}/{AUTHORS_DB_NAME}", 'wb') as dbfile:
            pickle.dump(authors, dbfile)
        return True
    except:
        return False
