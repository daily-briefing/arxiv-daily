from .helpers import api
from .helpers import db
from .helpers import html
import os
import datetime


class ArxivDaily:
    def __init__(self, page=1, n_new_papers=10):
        self.new_papers = api.get(page=page, length=n_new_papers)

        # TODO: backup
        papers_ok, self.papers = db.get_papers()
        authors_ok, self.authors = db.get_authors()

        self.relevant = {}

        self.rel_keywords = []
        self.rel_authors = []

    def _in_summary(self, summary, keywords):
        for k in keywords:
            if k in summary:
                return True

        return False

    def _in_authors(self, authors_list, authors):
        for a in authors:
            if a in authors_list:
                return True

        return False

    def find_relevant(self, keywords, authors):
        self.rel_keywords = keywords
        self.rel_authors = authors
        for paper in self.new_papers:
            if self._in_summary(paper['summary'], keywords):
                self.relevant[paper['id']] = paper

            if self._in_authors(paper['authors'], authors):
                self.relevant[paper['id']] = paper

        return len(self.relevant)

    def store_relevant(self):
        successfull = True

        for pid, p in self.relevant.items():
            for a in p['authors']:
                if a in self.authors:
                    self.authors[a]['papers'].append(pid)
                else:
                    self.authors[a] = {'papers': [pid]}

            if pid in self.papers:
                if p['version'] not in self.papers[pid]:
                    self.papers[pid][p['version']] = p
            else:
                self.papers[pid] = {p['version']: p}

        if not db.store_papers(self.papers):
            print('Unable to store papers!')
            successfull = False

        if not db.store_authors(self.authors):
            print('Unable to store authors!')
            successfull = False

        return successfull

    def email_me(self, email):
        path = html.write(
            list(self.relevant.values()), self.rel_keywords, self.rel_authors
        )
        time = datetime.date.today().strftime('%d, %b %Y')
        os.system(
            f"command -v mutt >/dev/null && $(cat {path} | mutt  -e \"set content_type=text/html\" -s \"arXiv Daily Briefing {time}\" -- {email})")
