#!/usr/bin/env sh

wd=$(dirname "$(readlink -f "$0")");
cd "$wd"

mkdir -p reports

./arxiv_it <REPLACE-ME-WITH-YOUR-EMAIL-ADDRESS> keywords.csv authors.csv -n 128 >> ./reports/arxiv_$(date +%F).txt

echo "sh ./run_arxiv_daily.sh" | at 7:00 AM tomorrow
