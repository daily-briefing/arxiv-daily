# arXiv daily briefing

# Usage

1. Clone this repository and change directory to it:
```bash
git clone https://gitlab.com/daily-briefing/arxiv-daily && cd arxiv-daily
```

1. Install the package by running:
```python
pip3 install -e . --user
```

1. Change directory to `scripts`.

1. Open and edit `authors.csv` (you can edit this file at anytime, it will take effect in the next run.).

1. Open and edit `keywords.csv` (same as `authors.csv`, you can edit this file at anytime).

1. Open and edit `run_arxiv_daily.sh`. Make sure your email address is correct.

1. Run `run_arxiv_daily.sh` script in a machine with SMTP set and `mutt` and `at` installed,
```bash
./run_arxiv_daily.sh
```

The last command would create a daily job, which send papers to your email address every day at about 7 AM (local time).

If you want to run the script directly, instead of setting a daily job, you can simply run the following command after changing directory to `scripts`:
```bash
./arxiv_it <your-email-address> <path-to-keywords.csv-file> <path-to-authors.csv-file> -n <number-of-papers-to-be-checked>
```

for instance,
```bash
./arxiv_it john@doe.com ./keywords.csv ./authors.csv -n 100
```

You can update `keywords.csv` and `authors.csv` files at any time.

Enjoy!


# Troubleshooting

## Not receiving any emails!

- In case you do not receive an email after running `arxiv_it`, check `${HOME}/.arxiv_daily/html` directory. If there are recently created HTML files, either your SMTP is not properly set up, or `mutt` command is not working correctly. You still can access the content of `${HOME}/.arxiv_daily/html` by running a simple HTTP server, like:

```bash
python3 -m http.server 1234
```

and opening `http://localhost:1234` in your browser.

- If you do not see any html files in your `${HOME}/.arxiv_daily/html` directory, please open an issue [here](https://gitlab.com/daily-briefing/arxiv-daily/-/issues). Thanks.
