import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="arxiv-daily",  # Replace with your own username
    version="0.0.1",
    author="Saeed Sarpas",
    author_email="s.sarpas@gmail.com",
    description="Retrieving and analysing recent arxiv papers",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/daily-briefing/arxiv-daily.git",
    #  packages=setuptools.find_packages(),
    packages=['arxiv_daily'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: GNU GENERAL PUBLIC LICENSE",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.0',
    scripts=[
        'scripts/arxiv_it',
    ],
    install_requires=[
        'requests',
        'xmltodict',
        'pathlib',
        'datetime',
        'beautifulsoup4',
    ],
)
